# Assignment A03Chandaluri
Adding Node, Express, BootStrap, EJS to our personal page.

WEBAPPA02 Assignment

This Website contains about university and student details.

Four tabs are visible in the nav bar.
First tab, university video and image icons.
Second tab, about me.
Third tab, Student registration form.
Fourth tab, Query/Suggestions.

### What is this repository for? ###

* Quick summary
* Version
* Github Page: https://github.com/S530742/A03Chandaluri

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Java Script code ###

* Created java script code for gpa calculator.

## How to use
Clone Repo
Run npm install to install all the dependencies in the package.json file.
Run node server.js to start the server. (Hit CTRL-C to stop.)
```
> npm install
> node sever.js
```
Point your browser to `http://localhost:8081. 